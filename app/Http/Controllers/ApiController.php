<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Items;
use Illuminate\Database\QueryException;

class ApiController extends Controller
{
    public function store(Request $request)
    {
        $xml = simplexml_load_string($request->getContent());

        $items = new Items();

        $items->uuid = $xml->uuid;
        $items->name = $xml->name;
        $items->amount = $xml->amount;
        $items->price = $xml->price;

        try {
            if ($items->save()) {
                return response()->json(['message' => 'Data saved successfully'], 201);
            }
        } catch (QueryException $e) {
            $errorMessage = $e->getMessage();

            return response()->json(['error' => 'Error during query execution: ' . $errorMessage], 500);
        }
    }
}
