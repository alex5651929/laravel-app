<?php

namespace App\Http\Controllers;


use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use App\Items;

class ItemsController extends Controller
{
    protected $data = [
        ['id' => '4', 'uuid' => 'a85b44fb-034c-11ed-8c9c-005056a0b35f', 'name' => 'Бетоносмеситель SKIPER  CM-70 (70л., 375Вт., 220В)', 'amount' => 37, 'price' => 598.00, 'created_at' => '2022-05-16 10:26:26', 'updated_at' => '2022-07-13 10:51:11'],
        ['id' => '13', 'uuid' => '73fce471-988c-11ec-9db7-005056a0b35f', 'name' => 'Виброплита SKIPER C120 (Honda GX200, 6.5л.с., плита 58х50см., 20кН, 35м/мин, колёса)', 'amount' => 17, 'price' => 2592.00, 'created_at' => '2022-05-16 10:26:27', 'updated_at' => '2022-07-13 10:51:11'],
        ['id' => '46', 'uuid' => '91870352-665a-11ed-9f69-000c290cff57', 'name' => 'Электростанция BRADO LT4000B (2.8кВт, 2х230В/16А/2.5кВт, 12В, бак 15.0л)', 'amount' => 57, 'price' => 778.00, 'created_at' => '2022-05-16 10:26:27', 'updated_at' => '2022-07-13 10:51:11'],
        ['id' => '79', 'uuid' => 'ba8dbc6e-3820-11ed-b5f8-005056a0b35f', 'name' => 'Бензопила KATANA GS7000 (40 см/16", 0.325", 1.5 мм, 64 зв. 2.3 кВт, 45 см3)', 'amount' => 144, 'price' => 291.00, 'created_at' => '2022-05-16 10:26:28', 'updated_at' => '2022-07-13 10:51:12'],
        ['id' => '94', 'uuid' => '9337a00c-9fbd-11ec-b6ed-005056a0b35f', 'name' => 'Газонокосилка электрическая SKIPER EL3215 (1500 Вт, шир.32 см, выс. 20-50 мм (3 поз)', 'amount' => 10, 'price' => 276.00, 'created_at' => '2022-05-16 10:26:29', 'updated_at' => '2022-07-13 10:51:12'],
        ['id' => '1580', 'uuid' => 'e3ae536c-2d1a-11ed-bf47-005056a0b35f', 'name' => 'Аккумуляторная дрель-шуруповерт KATANA PROdrive 1820 (29.04.22;новый, не комплект)', 'amount' => 1, 'price' => 91.50, 'created_at' => '2022-05-16 10:26:46', 'updated_at' => '2022-07-13 10:51:27'],
    ];

    public function index() {
        $items = DB::table('items')->get();

        return view('items', ['items' => $items]);
    }

    public function create()
    {
        $result = [];

        foreach ($this->data as $item) {
            $items = new Items();

            $items->id = $item['id'];
            $items->uuid = $item['uuid'];
            $items->name = $item['name'];
            $items->amount = $item['amount'];
            $items->price = $item['price'];
            $items->created_at = $item['created_at'];
            $items->updated_at = $item['updated_at'];

            try {
                if ($items->save()) {
                    $result[] = 'item id ' . $items->id . ' => saved successfully!';
                }
            } catch (QueryException $e) {
                $errorMessage = $e->getMessage();
                $result[] = 'item id ' . $items->id . ' => ' . $errorMessage;
            }
        }

        return response()->json(['result' => $result], 201);
    }
}
