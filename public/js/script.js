'use strict';

$(document).ready(function () {
   const btn = $( "button" );

   btn.on( "click", function() {
      $(".container").toggle();
      btn.text() === 'Hide table' ? btn.html("Show table") : btn.html("Hide table");
   });
});
