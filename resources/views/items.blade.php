@extends('layout')

@section('title', 'Список товаров')

@section('content')

    <div class="item">ID</div>
    <div class="item">UIDI</div>
    <div class="item">NAME</div>
    <div class="item">AMOUNT</div>
    <div class="item">PRICE</div>
    <div class="item">CREATED_AT</div>
    <div class="item">UPDATED_AT</div>

    @foreach ($items as $item)
        <div class="item">{{ $item->id }}</div>
        <div class="item">{{ $item->uuid }}</div>
        <div class="item">{{ $item->name }}</div>
        <div class="item">{{ $item->amount }}</div>
        <div class="item">{{ $item->price }}</div>
        <div class="item">{{ $item->created_at }}</div>
        <div class="item">{{ $item->updated_at }}</div>
    @endforeach

@endsection
