<!DOCTYPE html>
<html>
<head>
    <title>@yield('title')</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/styles.css') }}">
    <script src="https://code.jquery.com/jquery-3.7.0.slim.min.js" integrity="sha256-tG5mcZUtJsZvyKAxYLVXrmjKBVLd6VpVccqz/r4ypFE=" crossorigin="anonymous"></script>
    <script src="{{ asset('/js/script.js')  }}"></script>
</head>
<body>

<h1>Список товаров</h1>
<div class="container">
    @yield('content')
</div>

<div class="btn">
    <button type="button" class="button" value="">Hide table</button>
</div>

</body>
</html>