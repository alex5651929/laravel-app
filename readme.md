## Инструкция для запуска проекта

1. в docker-compose.yml установить свои значения для user и uid.
2. docker compose up -d
3. docker exec -it laravel-app_app_1 bash
4. composer install
5. php artisan migrate
6. php artisan db:seed --class=FirstUserSeeder
7. http://localhost:8000/items/create - добавить товары
8. http://localhost:8000/items - вывести на страницу список товаров 
9. request.http - проверить работу запроса к api
